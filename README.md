# Dev Environment Boilerplate

## SK Consult LLC Limited

### Description

Boiler Plate Environment for ```PHP``` Applications

Version:
> 0.0.1

License:
> GNUv3

Docker Stack: 
> * NGINX
> * PHP
> * MySQL

### Install

1. Clone the project to your work folder

```
$ git clone https://skostadinov@bitbucket.org/skostadinov/docker-php-mysql-nginx-boilerplate.git <Project_Name>

$ cd <Project_Name>
```

2. Remove the existing git folder. 
```
$ rm -rf .git 

```

3. Start Docker Compose
```
$ sudo docker-compose build && docker-compose up -d
```

you should now be able to access the server on ```http://localhost:8088```

